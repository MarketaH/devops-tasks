FROM python:3.9-slim

# Prejdi do slozky
WORKDIR /opt/devops-tasks

# Nakopirovani a instalace zavislosti
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

# Nakopirovani aplikace
COPY clinic/ ./

COPY docker-entrypoint.sh .
ENTRYPOINT ["./docker-entrypoint.sh"]

# Prikaz pro spusteni
CMD ["gunicorn", "clinic.wsgi"]